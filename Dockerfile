FROM docker.io/archlinux

RUN pacman -Syuq --noconfirm \
  git \
  ffmpeg \
  fontconfig \
  pdftk \
  unzip \
  nspr \
  pango \
  xorg-server-xvfb \
  nss \
  wget \
  curl \
  dbus \
  nodejs-lts-fermium \
  chromium \
  yarn

WORKDIR /home/ltmuser/workspace

RUN groupadd -r ltmuser && useradd -r -g ltmuser -G audio,video ltmuser \
  && mkdir -p /home/ltmuser/Downloads \
  && mkdir -p /home/ltmuser/workspace \
  && chown -R ltmuser:ltmuser /home/ltmuser

# USER ltmuser

ENTRYPOINT /bin/sh